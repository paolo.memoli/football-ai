from tinydb import TinyDB, Query, storages
from tinydb.middlewares import CachingMiddleware
import datetime
import pprint

atts_list = [
    'Pace',
    'Strength',
    'Jumping',
    'Stamina',
    'Agility',
    'Balance',
    'Resistance',
    'Reflexes',
    'Positioning',
    'Anticipation',
    'Leadership',
    'Agressivity',
    'Fighting Spirit',
    'Composure',
    'Rigour',
    'Game vision',
    'Behavior',
    'Compliance',
    'Handling',
    'Kicking1',
    'Throwing1',
    'Rushing Out',
    'Aerial Ability',
    'Free Kicks',
    'Shot power',
    'Ball Control',
    'Technique',
    'Dribbling',
    'Heading',
    'Right foot',
    'Left foot'
]

# import glob
pp = pprint.PrettyPrinter(indent=2)

CachingMiddleware.WRITE_CACHE_SIZE = 100

db_games = TinyDB('data/games.json', storage=CachingMiddleware())
db_players = TinyDB('data/players.json', storage=CachingMiddleware())

Game = Query()
games = db_games.all()

Player = Query()
players = db_players.all()


def get_att(player, att):
    for player_att in player['atts']:
        if (player_att.keys()[0] == att):
            return player_att.values()[0]

def find_player(player_url):
    for player in players:
        if (player['url'] == player_url):
            return player
    return None

for player in players:
    if ((player['atts']) and ('skill' in player['atts'][0])):
        new_atts=[]
        for unchecked_att in player['atts']:
            if (unchecked_att['skill'] in atts_list):
                for exsisting_att in new_atts:
                    if (unchecked_att['skill'] in exsisting_att):
                        new_atts.remove(exsisting_att)
                new_atts.append({ unchecked_att['skill']: unchecked_att['value'] })
        print 'Updating player:', db_players.update({'atts': new_atts}, eids=[player.eid])

for game in games:
    if (not('accuracy' in game)):
        accuracy = 0
        try:
            if (('players' in game['ta']) and ('players' in game['th'])):
                for player in (game['ta']['players'] + game['th']['players']):
                    player_object = find_player(player['url'])
                    if ('atts' in player_object):
                        for player_att in player_object['atts']:
                            if(player_att.values()[0] > 0):
                                accuracy = accuracy + 1
        except Exception as e:
            print e, game['url']
        print 'Updating game accuaracy:', accuracy, db_games.update({ 'date': 2017, 'accuracy': accuracy }, eids=[game.eid])
db_games.close()
