import numpy as np
import scipy.misc

from tinydb import TinyDB, Query, storages
from tinydb.middlewares import CachingMiddleware

np.random.seed(100)

from keras.layers import Conv2D, MaxPooling2D, Input, Dense, Flatten, add, Multiply, Dropout, concatenate
from keras.models import Model
from keras.utils import np_utils
from keras.layers import TimeDistributed
from keras import backend as K
from keras.models import load_model

CachingMiddleware.WRITE_CACHE_SIZE = 500

target_accuracy = 500

atts_list = [
    'Pace',
    'Strength',
    'Jumping',
    'Stamina',
    'Agility',
    'Balance',
    'Resistance',
    'Reflexes',
    'Positioning',
    'Anticipation',
    'Leadership',
    'Agressivity',
    'Fighting Spirit',
    'Composure',
    'Rigour',
    'Game vision',
    'Behavior',
    'Compliance',
    'Handling',
    'Kicking1',
    'Throwing1',
    'Rushing Out',
    'Aerial Ability',
    'Free Kicks',
    'Shot power',
    'Ball Control',
    'Technique',
    'Dribbling',
    'Heading',
    'Right foot',
    'Left foot'
]



db_games = TinyDB('data/games.json', storage=CachingMiddleware())
db_players = TinyDB('data/players.json', storage=CachingMiddleware())

Game = Query()
games = db_games.all()

Player = Query()
players = db_players.all()

homeInput = Input(shape=(11,31,1))
awayInput = Input(shape=(11,31,1))

# a layer instance is callable on a tensor, and returns a tensor

home = Conv2D(5, 2, activation="tanh")(homeInput)
home = MaxPooling2D(2, padding="same")(home)
home = Dense(5, activation='tanh')(home)

away = Conv2D(5, 2, activation="tanh")(awayInput)
away = MaxPooling2D(2, padding="same")(away)
away = Dense(5, activation='tanh')(away)

x = add([home, away])

x = Dropout(0.1)(x)

x = Dense(10, activation='tanh')(x)

x = Flatten()(x)

x = Dense(10, activation='tanh')(x)

Score = Dense(3, activation="sigmoid", name="ScoreOutput")(x)

model = Model(inputs=[homeInput, awayInput], outputs=Score )

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])


# file_data = np.load("data/2017_all.npz")
# th = file_data['th']
# ta = file_data['ta']
# sf = file_data['sf']

def get_att(player, att):
    for player_att in player['atts']:
        if (player_att.keys()[0] == att):
            return player_att.values()[0]

def get_average(team_players, att):
    total_att = 0
    total_players = 0
    for player in team_players:
        player_att = get_att(find_player(player['url']), att)
        # print player_att
        if (player_att != None):
            total_att = total_att + player_att
            total_players = total_players + 1
    if (total_players > 0):
        return total_att/total_players
    else:
        # FIND BIGGER AVERAGE
        return 0

def find_player(player_url):
    for player in players:
        if (player['url'] == player_url):
            return player
    return None

def make_team_array(team):
    team_array = np.empty((0,31), dtype=np.int8)
    for player_ref in team:
        player = find_player(player_ref['url'])
        player_atts = []
        for att in atts_list:
            player_att = get_att(player, att)
            if (player_att):
                player_atts.append(player_att)
            else:
                player_atts.append(get_average(team, att))

        team_array =  np.append(team_array, np.array([player_atts]), axis=0)
    return team_array

ath = np.empty((0, 11, 31), dtype=np.int8 )
ata = np.empty((0, 11, 31), dtype=np.int8 )

ash = np.empty((0, 1), dtype=np.int8 )
asa = np.empty((0, 1), dtype=np.int8 )

asc = np.empty((0, 3), dtype=np.int8 )

counter = 0
for game in games:
    if (game['accuracy'] > target_accuracy):

        th = make_team_array(game['th']['players'])
        ta = make_team_array(game['ta']['players'])
        sh = game['th']['score']
        sa = game['ta']['score']

        sc = 1
        if (sh > sa):
            sc = 0
        elif (sa > sh):
            sc = 2
        sc = np_utils.to_categorical(sc, 3)

        ath =  np.append(ath, [th], axis=0)
        ata =  np.append(ata, [ta], axis=0)

        ash = np.append(ash, [[sh]], axis=0)
        asa = np.append(asa, [[sa]], axis=0)

        asc = np.append(asc, sc, axis=0)

        counter = counter + 1

        print ath.shape, ata.shape

        print 'done game:', counter,  game['url']

    if (counter == 20):
        counter = 0
        np.savez_compressed("data/2017_all", ath=ath, ata=ata, asc=asc )
        hist = model.fit([np.expand_dims(ath, 3), np.expand_dims(ata, 3)], asc, batch_size=200, epochs=5000, verbose=1)



#
# th = (np.expand_dims(tha, 3))
# ta = (np.expand_dims(taa, 3))
#
# sfc = np_utils.to_categorical(sf, 3)

# th = th[:games, :, :]
# ta = ta[:games, :, :]
# sfc = sfc[:games, :]

# model_path = 'model_2017.h5'
#
# for x in range(0, 200):
#     try:
#         model = load_model(model_path)
#     except Exception as e:
#         print e
#     hist = model.fit([th, ta], sfc, batch_size=200, epochs=50, verbose=1)
#     model.save(model_path)


# print(hist.history)
