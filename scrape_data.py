from lxml import html
import requests
import dateutil.parser
import pprint
import os
import random

import datetime
import time

from tinydb import TinyDB, Query

# Variables ================

payload = {
    'login': os.environ['FOOT_LOGIN'],
    'password': os.environ['FOOT_PASSWORD'],
    'crealog':1,
    'connect.x':54,
    'connect.y':12
}

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

base_url = 'http://www.footballdatabase.eu/'
# base_url = 'http://lnbbb.ankttygfqqifyfgfxj.cnjz.se2.gsr.awhoer.net/'

wait_time = (1)

proxies = [
    'http://202.143.113.162:8080',
    'http://139.59.60.181:3128',
    'http://159.203.34.113:3128',
    # 'http://217.182.158.159:80',
    # 'http://138.197.145.77:3128',
    # 'http://202.71.153.201:8080',
    # 'http://70.95.8.189:30657',
    # 'http://118.172.118.26:8080',
    # 'http://213.124.171.105:80',
    # 'http://178.62.68.75:3128',
    # 'http://106.185.24.64:8080',
    # 'http://143.255.142.31:8080',
    # 'http://35.154.54.218:8080',
    # 'http://171.4.129.214:8080'
]

db_games = TinyDB('data/games.json')
db_players = TinyDB('data/players.json')

pp = pprint.PrettyPrinter(indent=2)

# Init ================

years = sorted(range(1998, 2018), reverse=True)
months = range(1,13)
days = range(1,32)

players = range(1,12)

atts_list = [
    'Pace',
    'Strength',
    'Jumping',
    'Stamina',
    'Agility',
    'Balance',
    'Resistance',
    'Reflexes',
    'Positioning',
    'Anticipation',
    'Leadership',
    'Agressivity',
    'Fighting Spirit',
    'Composure',
    'Rigour',
    'Game vision',
    'Behavior',
    'Compliance',
    'Handling',
    'Kicking1',
    'Throwing1',
    'Rushing Out',
    'Aerial Ability',
    'Free Kicks',
    'Shot power',
    'Ball Control',
    'Technique',
    'Dribbling',
    'Heading',
    'Right foot',
    'Left foot'
]

total_games = 0;
total_players = 0;

def crawl (url):
    global wait_time, payload, proxies
    active_proxy = {
        'http' : random.choice(proxies)
    }
    time.sleep(wait_time)
    tree = None
    try:
        page = requests.post(url, data=payload, headers=headers, proxies=active_proxy)
        # page = requests.post(url, data=payload, headers=headers)
        tree = html.fromstring(page.content)
    except Exception as e:
        print ("HTTP Error pulling page:", url, active_proxy)
        active_proxy = {
            'http' : random.choice(proxies),
        }
        raise

    try:
        (tree.cssselect('a.link_8')[9].text_content() == "Make a donation")
        return tree
    except Exception as e:
        print ("Non-data page", url, active_proxy)
        raise


def lookup_game(game_url):
    Game = Query()
    if (db_games.get(Game.url == game_url)):
        global total_players
        total_players = total_players + 22
        print "Year:", year, "Month:", month, "Days:", day, " - Games:", total_games, "Players:", total_players
    else:
        try:
            tree = crawl(game_url)

            th = {}
            th['name'] = tree.cssselect('a.link_4')[0].text_content()
            th['score'] = int(tree.cssselect('.grosscore')[0].text_content())
            th['players'] = []

            ta = {}
            ta['name'] = tree.cssselect('a.link_4')[1].text_content()
            ta['score'] = int(tree.cssselect('.grosscore')[1].text_content())
            ta['players'] = []

            game_round = tree.cssselect('.definitionbleu')[1].text_content().split(" ")[1]

            date_string =  tree.cssselect('.definitionbleu')[1].text_content().split(" Date ")[1]
            date = date_string

            league_string = tree.cssselect('.definitionbleu')[0].text_content()[:-10]

            if (len(tree.cssselect('#stong1')) == 0):
                th['players'] = None
                ta['players'] = None
            else:
                home_player = True

                for player in players:
                    global total_players
                    print "Year:", year, "Month:", month, "Days:", day, "- Games:", total_games, "Players:", total_players
                    total_players = total_players + 1
                    for element in tree.cssselect('#stong1 tr:nth-child('+ str(player) +') a.link_5'):
                        raw_path = element.get("href")
                        if (raw_path):

                            player_url = base_url + raw_path
                            try:
                                lookup_player(player_url)
                            except Exception as e:
                                print e, player_url

                            player = {}
                            player['name'] = element.text_content()
                            player['url'] = player_url

                            if home_player:
                                th['players'].append(player)
                            else:
                                ta['players'].append(player)
                        home_player = not home_player

            game_object = ({
                'th': th,
                'ta': ta,
                'date': date,
                'url': game_url,
                'round': game_round,
                'league': league_string
            })

            db_games.insert(game_object)
        except Exception as e:
            print e



def lookup_player(player_url):
    Player = Query()
    if not(db_games.get(Player.url == player_url)):
        try:
            tree = crawl(player_url)

            name = tree.cssselect('.titrefiche')[0].text_content()
            club = {}

            try:
                raw_club =  tree.cssselect('.link_3')[1].get("href")
                club['name'] = tree.cssselect('.link_3')[1].text_content()
                club['url'] = base_url + raw_club
            except Exception as e:
                club['url'] = None
                club['name'] = None

            atts=[]
            for element in tree.cssselect('.tabmodulebleu250 tr'):
                for att_name in atts_list:
                    if att_name in element.text_content():
                        skill = element.text_content().split('1234567891011121314151617181920')[1][:2]
                        try:
                            att = {att_name: int(skill)}
                            if (att_name in exsisting_att):
                                atts.remove(exsisting_att)
                            atts.append(att)

                        except Exception as e:
                            print "no skill value found:", player_url


            player_object = ({
                'name': name,
                'url': player_url,
                'club': club,
                'atts': atts
            })

            # pp.pprint(player_object)
            db_players.insert(player_object)
        except Exception as e:
            print e


# lookup_game(base_url + 'football.coupe.chili.croatie.214325.en.html')
# lookup_game(base_url + 'football.match.etincelles-gisenyi.marines-gisenyi.1184944.en.html')
#
# exit()

for year in years:
    for month in months:
        for day in days:
            page_url = base_url + 'calendrierclub.php?date=' + str(year) + '-' + str(month) + '-' + str(day)
            try:
                tree = crawl(page_url)
                game_url_prev = ""
                for element in tree.cssselect('.score'):
                    total_games = total_games + 1
                    raw_path = element.get("onclick")
                    if (raw_path):
                        game_url = base_url + raw_path.split("'")[1]
                        if (game_url != game_url_prev):
                            game_url_prev = game_url
                            try:
                                lookup_game(game_url)
                            except Exception as e:
                                print e, game_url
            except Exception as e:
                print e
