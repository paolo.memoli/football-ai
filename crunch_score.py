import numpy as np
import scipy.misc

np.random.seed(100)

from keras.layers import Conv2D, MaxPooling2D, Input, Dense, Flatten, add, Multiply, Dropout
from keras.models import Model
from keras.utils import np_utils
from keras.layers import TimeDistributed
from keras import backend as K

homeInput = Input(shape=(16,30,1))
awayInput = Input(shape=(16,30,1))

# a layer instance is callable on a tensor, and returns a tensor

# home = Conv2D(10, 2, kernel_initializer="uniform", activation="relu")(homeInput)
home = Dense(10, kernel_initializer="normal", activation="tanh")(homeInput)

# away = Conv2D(10, 2, kernel_initializer="uniform", activation="relu")(awayInput)
away = Dense(10, kernel_initializer="normal", activation="tanh")(awayInput)

x = add([home, away])
x = Flatten()(x)

# x = Dense(20, kernel_initializer="uniform", activation="relu")(x)
x = Dense(10, kernel_initializer="normal", activation="tanh")(x)

homeScore = Dense(1, kernel_initializer="normal", activation="relu")(x)
awayScore =  Dense(1, kernel_initializer="normal", activation="relu")(x)

model = Model(inputs=[homeInput, awayInput], outputs=[homeScore, awayScore] )

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

file_data = np.load("data/all_score.npz")
th = file_data['th']
ta = file_data['ta']
sh = file_data['sh']
sa = file_data['sa']
hist = model.fit([(np.expand_dims(th, 3)), (np.expand_dims(ta, 3))], [sh, sa], batch_size=500, epochs=1000, verbose=1)



# print(hist.history)
